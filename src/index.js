const emptyIpBin = Array(4).fill(Array(8).fill('<span class="bd">&bull;</span>').join('')).join(' ')
const getDetails = () => ({
    d1: document.getElementById('d1'),
    d2: document.getElementById('d2'),
    d3: document.getElementById('d3'),
    d4: document.getElementById('d4'),
    d5: document.getElementById('d5'),
    d6: document.getElementById('d6'),
})

document.addEventListener('DOMContentLoaded', () => {
    document.querySelectorAll('.tab').forEach(x => {
        x.onclick = () => {
            document.querySelectorAll('.tab').forEach(y => y.classList.remove('active'))
            x.classList.add('active')
            document.querySelectorAll('.tabs-container > *').forEach(y => y.style.display = 'none')
            document.getElementById(x.dataset.id).style.display = 'block'
        }
    })
})

function analyze(e) {
    if (typeof IpAddr === 'undefined') return console.warn('Skipping analyze() because core.js has not been loaded')
    ipRegex.lastIndex = 0
    const el = e.target, d = getDetails()
    const val = el.value, valid = ipRegex.test(val)
    const parts = val.split('/')
    el.style.color = el.style.borderColor = valid ? '#37cb37' : '#e11818'
    if (valid) {
        const ip = new IpAddr(parts[0], parts[1]), prefixed = (parts.length === 2)
        d.d1.innerHTML = parts[0]
        d.d2.innerHTML = prefixed ? parts[1] : '&mdash;'
        d.d3.innerHTML = `${ip.type} &dash; ${ip.private ? 'Private' : 'Public'}`
        d.d4.innerHTML = prefixed ? ip.mask : '&mdash;'
        d.d5.innerHTML = prefixed ? ip.rangeStr.join('<br>') : '&mdash;'
        d.d6.innerHTML = prefixed ? ip.broadcast.toString() : '&mdash;'

        typeof updateTab1 !== 'undefined' && updateTab1(ip, prefixed)
        typeof updateTab2 === 'undefined' && updateTab2(ip, prefixed)
    }
}
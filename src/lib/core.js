const ipRegex = /^(\b25[0-5]|\b2[0-4][0-9]|\b[01]?[0-9][0-9]?)(\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)){3}(\/\b([0-9]|[12][0-9]|3[0-2])\b)?$/g

const IpUtil = {
    parse: (s) => s.split('.').reduce((ipInt, octet) => { return (ipInt<<8) + parseInt(octet, 10)}, 0) >>> 0,
    toString: (i) => `${i >>> 24}.${i >> 16 & 255}.${i >> 8 & 255}.${i & 255}`,
    toStringB: (i) => {
        const m = n => n.toString(2).padStart(8, '0')
        const o = `${m((i >> 24) & 0xFF)}${m((i >> 16) & 0xFF)}${m((i >> 8) & 0xFF)}${m(i & 0xFF)}`
        return o.match(/.{1,8}/g).join(' ')
                .replace(/1/g, '<span class="b1">1</span>')
                .replace(/0/g, '<span class="b0">0</span>')
    },
    prefixLength: (ip1, ip2) => {
        if (ip2 <= ip1) return 0;
        let mask = ip1 & ip2
        let zero_bits;
        for (zero_bits = 0; (mask & 1) == 0; mask >>= 1, zero_bits++);
        return 32 - zero_bits
    },
    STATIC_PRIVATE_SPACES: [
        [0, -16777216],
        [2130706432, -16777216],
        [167772160, -16777216],
        [2886729728, -1048576],
        [3232235520, -65536]
    ]
}
class IpAddr {
    constructor(ip, prefix) {
        this._ipraw = (typeof ip === 'number') ? IpUtil.toString(ip) : ip
        this._ip = (typeof ip === 'number') ? ip : IpUtil.parse(ip)
        this._prefixed = (typeof prefix === 'string')
        if (this._prefixed) {
            this._prefix = Number(prefix)
            this._mask = (0xFFFFFFFF << (32 - this._prefix)) & 0xFFFFFFFF
        }
    }
    toString() {
        return `${this.ip >>> 24}.${this.ip >> 16 & 255}.${this.ip >> 8 & 255}.${this.ip & 255}`
    }
    get ip() { return this._ip }
    get prefix() { return this._prefix }
    get mask() {
        if (!this._prefixed) return undefined
        if (this._prefix <= 0) return '0.0.0.0'
        return `${(this._mask >> 24) & 0xFF}.${(this._mask >> 16) & 0xFF}.${(this._mask >> 8) & 0xFF}.${this._mask & 0xFF}`
    }
    get private() {
        const ip = this._ip
        return IpUtil.STATIC_PRIVATE_SPACES.some(([net, mask]) => (net & mask) == (ip & mask))
    }
    get type() {
        if (!this._prefixed) {
            return this._ipraw.endsWith('.255') ? 'Broadcast' : (this._ipraw.endsWith('.0') ? 'Network' : 'Host')
        } else {
            if ((this._ip | ~this._mask) === this._ip) return 'Broadcast'
            if ((this._ip & 0xFF) === 0) return 'Network'
            return 'Host'
        }
    }
    get range() {
        return this._prefixed ? [new IpAddr((this._ip & this._mask) + 1), new IpAddr((this._ip | ~this._mask) - 1)] : undefined
    }
    get rangeStr() {
        return this._prefixed ? [IpUtil.toString((this._ip & this._mask) + 1), IpUtil.toString((this._ip | ~this._mask) - 1)] : undefined
    }
    get broadcast() {
        return this._prefixed ? new IpAddr(this._ip | ~this._mask) : undefined
    }
}
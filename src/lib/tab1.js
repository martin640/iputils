function updateTab1(ip, prefixed) {
    const d = {
        d7: document.getElementById('d7'),
        d8: document.getElementById('d8'),
        d9: document.getElementById('d9'),
        d10: document.getElementById('d10'),
        d11: document.getElementById('d11'),
        d12: document.getElementById('d12'),
    }
    d.d7.innerHTML = IpUtil.toStringB(ip._ip)
    d.d8.innerHTML = prefixed ? IpUtil.toStringB(ip._mask, ip._prefix) : emptyIpBin
    d.d9.innerHTML = prefixed ? IpUtil.toStringB(ip._ip & ip._mask, ip._prefix) : emptyIpBin
    d.d10.innerHTML = prefixed ? IpUtil.toStringB(ip.broadcast._ip) : emptyIpBin
    d.d11.innerHTML = prefixed ? IpUtil.toStringB(ip.range[0]._ip) : emptyIpBin
    d.d12.innerHTML = prefixed ? IpUtil.toStringB(ip.range[1]._ip) : emptyIpBin
}